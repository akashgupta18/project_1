function problem6(inventory) {
    if (inventory == undefined || inventory.length == 0){
        return [];
    }
    let audi_and_bmw_cars = [];
    for (let i=0; i < inventory.length; i++){
        if (inventory[i]['car_make'] === "BMW" || inventory[i]['car_make'] === "Audi") {
            audi_and_bmw_cars.push(inventory[i])
        }
    }

    return audi_and_bmw_cars;
}

module.exports = problem6;

