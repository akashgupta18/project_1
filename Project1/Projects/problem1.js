function problem1(inventory, id_car) {
    if(inventory == undefined || inventory.length == 0 || id_car == undefined){
        return [];
    }
    for (let i=0; i < inventory.length; i++){
        if (inventory[i].id === id_car){
            return ['Car ' + id_car + ' is a ' + inventory[i]['car_year'] + " " + inventory[i]['car_model']];
        }
    }
}

module.exports = problem1;

