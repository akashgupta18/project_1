function problem5(inventory, year) {

    if (inventory == undefined || year == undefined || inventory.length == 0){
        return [];
    }
    let olderCars_less_than_2000 = [];

    for (let i=0; i < inventory.length; i++){
        if (inventory[i]['car_year'] < year){
            olderCars_less_than_2000.push(inventory[i])
        }
    }

    return olderCars_less_than_2000;
}

module.exports = problem5;

