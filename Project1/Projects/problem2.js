function problem2(inventory) {
    if (inventory == undefined || inventory.length == 0){
        return [];
    }

    let id_lastCar = inventory.length - 1;

    return ['Last car is a ' + inventory[id_lastCar]['car_make'] + " " + inventory[id_lastCar]['car_model']];

}

module.exports = problem2;

