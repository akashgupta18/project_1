function problem3(inventory) {
    if (inventory == undefined || inventory.length == 0){
        return [];
    }
    let carModels = [];
    for (let i=0; i < inventory.length; i++){
        carModels.push(inventory[i]['car_model']);
    }
    let sortedModels = carModels.sort((a,b) => a.localeCompare(b) );
    return sortedModels;
}

module.exports = problem3;

