function problem4(inventory) {
    if (inventory == undefined || inventory.length == 0){
        return [];
    }
    let carYears = [];
    
    for (let i=0; i < inventory.length; i++){
        carYears.push(inventory[i]['car_year']);
    }

    return carYears;
}

module.exports = problem4;

